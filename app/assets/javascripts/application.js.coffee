#= require jquery
#= require jquery_ujs
#= require iconic.min
#= require search
#= require turbolinks


Turbolinks.enableProgressBar()
Turbolinks.enableTransitionCache()

iconic = IconicJS pngFallback: 'assets/images/iconic'
# $('#invhead').fixedsticky()
# ($ document).on 'page:load', -

$(document).on 'page:change', ->
	# $('#invhead').fixedsticky()
	$('.toggle-click').click (e)->
		$('.funnel').toggle()
		iconic.inject '.iconic'

	$('.clickable-row').click (e)->
				return Turbolinks.visit($(this).find("a").first().attr("href"))

# $(document).on 'page:fetch', ->
# 		console.log 'fetch'
# 	a = window.setTimeout -> 
# 		$('.container').hide()
# 		$('.app-loader').show()
# 	, 1000	

# $(document).on 'page:receive', ->
# 		console.log 'receive!'
# 	window.clearTimeout(a)
# # 	a = undefined
# 	$('.container').show()
# 	$('.app-loader').hide()


GetViewedItem = ->
	pollEvery = 3000
	$.ajax '/invoices/1.json',
		type: 'GET',
		success: (data) ->
			console.log 'Poll!'
			console.log data
			setTimeout GetViewedItem, pollEvery



# poll()
