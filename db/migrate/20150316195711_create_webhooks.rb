class CreateWebhooks < ActiveRecord::Migration
  def change
    create_table :webhooks do |t|
      t.string :description
      t.string :url

      t.timestamps null: false
    end
  end
end
