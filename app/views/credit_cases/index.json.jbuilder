json.array!(@credit_cases) do |credit_case|
  json.extract! credit_case, :id, :name, :state, :user_id
  json.url credit_case_url(credit_case, format: :json)
end
