class CreateAdministrations < ActiveRecord::Migration
  def change
    create_table :administrations do |t|
      t.string :name
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
