json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :date, :user_id, :price, :invoice_number
  json.url invoice_url(invoice, format: :json)
end
