class AdministrationsController < ApplicationController
  before_action :set_administration, only: [:show, :edit, :update, :destroy]
  respond_to :html

  def index
    @administrations = Administration.all
    respond_with(@administrations)
  end

  def show
    respond_with(@administration)
  end

  def new
    @administration = Administration.new
    respond_with(@administration)
  end

  def edit
  end

  def create
    @administration = Administration.new(administration_params)
    @administration.save
    respond_with(@administration)
  end

  def update
    @administration.update(administration_params)
    respond_with(@administration)
  end

  def destroy
    @administration.destroy
    respond_with(@administration)
  end

  private
    def set_administration
      @administration = Administration.find(params[:id])
    end

    def administration_params
      params.require(:administration).permit(:name, :company_id)
    end
end
