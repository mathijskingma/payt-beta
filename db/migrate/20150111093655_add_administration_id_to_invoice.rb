class AddAdministrationIdToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :administration_id, :integer
  end
end
