class WebhookMailer < ApplicationMailer
	def webhook_failure(email, retries)
		@retries = retries + 1
		emails = []
		emails << email
		emails << 'm.kingma@payt.nl'
    mail(to: emails, subject: '[FAILED] Webhook')
	end
end
