class CreditCasesController < ApplicationController
  before_action :set_credit_case, only: [:show, :edit, :update, :destroy]
  skip_before_action :authenticate_user!
  # GET /credit_cases
  # GET /credit_cases.json
  def index
    @credit_cases = CreditCase.includes(:user)
  end

  # GET /credit_cases/1
  # GET /credit_cases/1.json
  def show
  end

  # GET /credit_cases/new
  def new
    @credit_case = CreditCase.new
  end

  # GET /credit_cases/1/edit
  def edit
  end

  # POST /credit_cases
  # POST /credit_cases.json
  def create
    @credit_case = CreditCase.new(credit_case_params)

    respond_to do |format|
      if @credit_case.save
        format.html { redirect_to @credit_case, notice: 'Credit case was successfully created.' }
        format.json { render :show, status: :created, location: @credit_case }
      else
        format.html { render :new }
        format.json { render json: @credit_case.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /credit_cases/1
  # PATCH/PUT /credit_cases/1.json
  def update
    respond_to do |format|
      if @credit_case.update(credit_case_params)
        format.html { redirect_to @credit_case, notice: 'Credit case was successfully updated.' }
        format.json { render :show, status: :ok, location: @credit_case }
      else
        format.html { render :edit }
        format.json { render json: @credit_case.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /credit_cases/1
  # DELETE /credit_cases/1.json
  def destroy
    @credit_case.destroy
    respond_to do |format|
      format.html { redirect_to credit_cases_url, notice: 'Credit case was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credit_case
      @credit_case = CreditCase.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def credit_case_params
      params.require(:credit_case).permit(:name, :state, :user_id)
    end
end
