class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.date :date
      t.integer :user_id
      t.decimal :price
      t.string :invoice_number

      t.timestamps null: false
    end
  end
end
