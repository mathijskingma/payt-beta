module Api::V1
	class InvoicesController < BaseController
		def index
			render json: Invoice.all
		end

		def create
			render json: {
				success: 1
			}
		end
	end
end