class CreateCreditCases < ActiveRecord::Migration
  def change
    create_table :credit_cases do |t|
      t.string :name
      t.string :state
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
