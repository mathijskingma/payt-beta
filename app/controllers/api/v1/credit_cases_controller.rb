module Api::V1
	class CreditCasesController < BaseController
		def index
			render json: CreditCase.all
		end
	end
end