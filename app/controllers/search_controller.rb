class SearchController < ApplicationController

	def search
	  if params[:search_query].nil?
	    @credit_cases = []
	  else
	    @credit_cases = CreditCase.search params[:search_query]
	  end
	end
end
