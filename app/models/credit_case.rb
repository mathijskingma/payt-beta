# == Schema Information
#
# Table name: credit_cases
#
#  id                :integer          not null, primary key
#  name              :string
#  state             :string
#  user_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  administration_id :integer
#

require 'elasticsearch/model'

class CreditCase < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  belongs_to :administration
  belongs_to :user

end