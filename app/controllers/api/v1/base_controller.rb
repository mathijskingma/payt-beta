module Api::V1
	class BaseController < ApplicationController
	  protect_from_forgery with: :null_session
		respond_to :json
    skip_before_action :authenticate_user!
		before_action :doorkeeper_authorize!
	end
end
