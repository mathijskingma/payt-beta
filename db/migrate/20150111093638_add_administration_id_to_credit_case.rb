class AddAdministrationIdToCreditCase < ActiveRecord::Migration
  def change
    add_column :credit_cases, :administration_id, :integer
  end
end
