require 'sidekiq/web'

Rails.application.routes.draw do
  resources :webhooks

  get 'dashboard/timeline'

  devise_for :users, controllers: { sessions: 'sessions' }
  authenticate :user do
    mount Sidekiq::Web => '/sidekiq'
  end
  use_doorkeeper

  devise_scope :user do
    get "/login" => "devise/sessions#new"
    get "/logout" => "devise/sessions#destroy"
  end

  # get '/', to: redirect('/', status: 302)

  get '/users/oauth/callback', to: 'users#callback'
  root 'credit_cases#index'
  get 'settings/webhooks', to: 'webhooks#index'
  resources :credit_cases

  # scope ':administration_id' do
  get '/', to: 'dashboard#timeline', as: :administration
  get 'search', to: 'search#search'
  localized do
    resources :invoices do
      resources :notes
    end
    resources :companies
    resources :credit_cases
    get 'settings', to: 'users#settings'
  end
  # end

  namespace :api do
    namespace :v1 do
      resources :credit_cases
      resources :invoices
      get '/me' => "credentials#me"
    end
  end
end
