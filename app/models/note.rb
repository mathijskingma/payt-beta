# == Schema Information
#
# Table name: notes
#
#  id         :integer          not null, primary key
#  content    :text
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  invoice_id :integer
#

class Note < ActiveRecord::Base
	belongs_to :invoice
	scope :desc, -> { order(created_at: :desc)}
end
