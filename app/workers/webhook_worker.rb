class WebhookWorker
  include Sidekiq::Worker
  sidekiq_options retry: 2, dead: false

  sidekiq_retries_exhausted do |msg|
  	WebhookMailer.webhook_failure('a.althoff@payt.nl', msg['retry_count']).deliver_now
    Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
  end

  def perform(url, object)
    RestClient.get 'http://localhost:3008/credit_cases.json'
  end
end
