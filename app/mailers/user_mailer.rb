class UserMailer < ApplicationMailer
  default from: "mathijskingma@gmail.com"

  def welcome_email(invoice)
  	@invoice = invoice
    mail(to: 'mathijskingma@gmail.com', subject: 'Welcome to My Awesome Site')
  end

end
