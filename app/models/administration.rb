# == Schema Information
#
# Table name: administrations
#
#  id         :integer          not null, primary key
#  name       :string
#  company_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Administration < ActiveRecord::Base
	belongs_to :company
	has_many :credit_cases
	has_many :invoices

	obfuscate_id
end
