class WebhookMailerPreview < ActionMailer::Preview
	def webhook_failure
		WebhookMailer.webhook_failure
	end
end