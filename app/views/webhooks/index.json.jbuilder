json.array!(@webhooks) do |webhook|
  json.extract! webhook, :id, :description, :url
  json.url webhook_url(webhook, format: :json)
end
