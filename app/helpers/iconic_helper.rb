module IconicHelper
  def iconic_tag(glyph, size = 'sm')
    src = image_path("iconic/#{glyph}-#{size}.svg")

    tag('img', class: "iconic", data: { src: src })
  end

  def iconic_file_tag(extension = 'pdf')
    src = image_path("iconic/file.svg")
    tag('img', class: "iconic", data: { src: src, file_extension: extension })
  end  
end