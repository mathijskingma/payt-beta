# == Schema Information
#
# Table name: invoices
#
#  id                :integer          not null, primary key
#  date              :date
#  user_id           :integer
#  price             :decimal(, )
#  invoice_number    :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  administration_id :integer
#

class Invoice < ActiveRecord::Base
	belongs_to :administration
	has_many :notes

  accepts_nested_attributes_for :notes
end
